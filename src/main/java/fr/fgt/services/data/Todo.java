package fr.fgt.services.data;

import java.io.Serializable;

import javax.ws.rs.FormParam;

public class Todo implements Serializable {
    @FormParam("text")
    private String text;

    @FormParam("done")
    private Boolean done;
    
    public Todo() {
        super();
    }

    public Todo(String text, Boolean done) {
        this.text = text;
        this.done = done;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

}
