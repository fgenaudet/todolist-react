package fr.fgt.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.fgt.services.data.Todo;

@Path("/rest/todos")
public class ServiceTodos {

    private static final String DB_KEY = "db";

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Todo> getTodos(@Context HttpServletRequest httpRequest) {
        final HttpSession session = httpRequest.getSession(true);
        if (session.getAttribute(DB_KEY) == null) {
            initDbInSession(session);
        }
        return getTodoListFromSession(session);
    }

    public List<Todo> getTodoListFromSession(final HttpSession session) {
        return (List<Todo>) session.getAttribute(DB_KEY);
    }

    @POST
    public List<Todo> addTodo(@BeanParam Todo todo, @Context HttpServletRequest httpRequest) {
        final HttpSession session = httpRequest.getSession(true);
        if (session.getAttribute(DB_KEY) == null) {
            session.setAttribute(DB_KEY, new ArrayList<Todo>());
        }
        final List<Todo> todoList = getTodoListFromSession(session);
        boolean alreadyExist = false;
        for (Todo todoLoop : todoList) {
            if (todoLoop.getText().equals(todo.getText())) {
                alreadyExist = true;
                break;
            }
        }
        if (!alreadyExist) {
            todoList.add(todo);
        }

        return todoList;
    }

    @PUT
    public List<Todo> updateTodo(@FormParam("key") String key, @FormParam("done") Boolean done,
            @Context HttpServletRequest httpRequest) {
        final HttpSession session = httpRequest.getSession(true);
        final List<Todo> todoList = getTodoListFromSession(session);
        for (Todo todo : todoList) {
            if (todo.getText().equals(key)) {
                todo.setDone(done);
                break;
            }
        }
        return todoList;
    }

    @DELETE
    public List<Todo> deleteTodo(@FormParam("key") String key, @Context HttpServletRequest httpRequest) {
        final HttpSession session = httpRequest.getSession(true);
        final List<Todo> todoList = getTodoListFromSession(session);
        Todo todoTodelete = null;
        for (Todo todo : todoList) {
            if (todo.getText().equals(key)) {
                todoTodelete = todo;
                break;
            }
        }
        if (todoTodelete != null) {
            todoList.remove(todoTodelete);
        }
        return todoList;
    }

    public void initDbInSession(final HttpSession session) {
        final ArrayList<Todo> todoList = new ArrayList<Todo>();
        todoList.add(new Todo("Get a new smartphone", true));
        todoList.add(new Todo("Write an article about React", true));
        todoList.add(new Todo("Get a raise", false));

        session.setAttribute(DB_KEY, todoList);
    }

}
