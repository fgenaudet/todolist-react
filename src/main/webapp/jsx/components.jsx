/** @jsx React.DOM */
var ReactTransitionGroup = React.addons.TransitionGroup;

var Todos = React.createClass({
	loadDataFromServer: function() {
		$.ajax({
      url: this.props.url,
      dataType: 'json',
      success: function(data) {
        this.setState({data: data});
      }.bind(this)
    });
	},
	handleTodoSubmit: function(todo) {
		var todos = this.state.data;
	    var newTodos = todos.concat([todo]);
	    this.setState({data: newTodos});
	    $.ajax({
	      url: this.props.url,
	      dataType: 'json',
	      type: 'POST',
	      data: todo,
	      success: function(data) {
	        this.setState({data: data});
	      }.bind(this)
	    });
	},
	handleTodoChecked: function(todo) {
		$.ajax({
		   url: this.props.url,
		   dataType: 'json',
		   type: 'PUT',
		   data: todo,
		   success: function(data) {
		        this.setState({data: data});
		   }.bind(this)
		});
	},
	handleTodoDelete: function(todo) {
		var todos = this.state.data;
		var selectedTodo = todos.filter(function(pTodo) {return pTodo.text == todo.key})[0];
		todos.splice(todos.indexOf(selectedTodo), 1);
		this.setState({data: todos});
		
		$.ajax({
		   url: this.props.url,
		   dataType: 'json',
		   type: 'DELETE',
		   data: todo,
		   success: function(data) {
		        this.setState({data: data});
		   }.bind(this)
		});
	},
	getInitialState: function() {
		return {data: []};
	},

	componentWillMount: function() {
		this.loadDataFromServer();
		setInterval(this.loadDataFromServer, this.props.pollInterval);
	},

	render: function() {
		return (
			<div className="todos">
				<h1>Todos</h1>
				<TodoList data={this.state.data} onTodoChecked={this.handleTodoChecked} onTodoDelete={this.handleTodoDelete}/>
				<TodoForm onTodoSubmit={this.handleTodoSubmit}/>
			</div>
		);
	}
});

var TodoList = React.createClass({
	render: function() {
		var that = this;
		var todos = this.props.data.map(function (todo) {
			return <Todo done={todo.done} onTodoChecked={that.props.onTodoChecked} onTodoDelete={that.props.onTodoDelete}>
				{todo.text}
			</Todo>;
	    });
		return (
			<div className="todo-list row">
				<ReactTransitionGroup transitionName="example">
					{todos}
				</ReactTransitionGroup>
			</div>
		);
	}
});

var Todo = React.createClass({
	handleTodoChecked: function() {
		var key = this.refs.done.props.key;
		var done = this.refs.done.getDOMNode().checked
		this.props.onTodoChecked({key: key, done: done});
	},
	handleTodoDelete: function() {
		var key = this.refs.done.props.key;
		this.props.onTodoDelete({key: key});
	},
	render: function() {
		var extraClass = this.props.done ? 'done' : '';
		var classes = 'todo col-xs-12 ' + extraClass;
		return (
			<div className={classes}>
				<p className="lead">
					{this.props.children}
				</p>
				<input type="checkbox" defaultChecked={this.props.done} onClick={this.handleTodoChecked} ref="done" key={this.props.children}/>
				<button className="btn btn-danger" onClick={this.handleTodoDelete}>Delete</button>
				</div>
		);
	}
});

var TodoForm = React.createClass({
	handleSubmit: function() {

		// Validate data
    var done = this.refs.done.getDOMNode().checked;
    var text = this.refs.text.getDOMNode().value.trim();
    if (!text) {
      return false;
    }
    this.props.onTodoSubmit({done: done, text: text});

		// Reset form
    this.refs.done.getDOMNode().checked = false;
    this.refs.text.getDOMNode().value = '';

		//Prevent default event
    return false;
  },
	render: function() {
		return (
			<div className="todo-form-div">
				<h1>Add a Todo</h1>
				<form className="todo-form form-inline" role="form" onSubmit={this.handleSubmit}>
					<div className="form-group">
						<label className="sr-only" htmlFor="text">Todo text</label>
						<input type="text" className="form-control" id="text" placeholder="Todo Text" ref="text"/>
					</div>
					<div className="checkbox">
						<label>
							<input type="checkbox" id="done" ref="done"/> Done
						</label>
					</div>
					<button type="submit" className="btn btn-default">Post</button>
				</form>
			</div>
		);
	}
});